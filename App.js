import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'react-native';
import { colors } from './src/assets/styles/Colors';
import StackNavigation from './src/navigation/stack/stackNavigation';
import BottomTab from './src/navigation/bottomTab/bottomTab';


const App = () => {
  return (
    <NavigationContainer>
      <StatusBar
        backgroundColor={colors.darkGrey}
        barStyle="light-content"
      />
      <StackNavigation />
    </NavigationContainer>
  );
};

export default App;