import React from 'react'
import HomeScreen from './home'
import Images from '../../../assets/styles/Images';


const Home = () => {
  const newsArr = [
    {
      id: '1',
      image: Images.flower_img1,
      heading: "lorem Ipsum is simply dummy1"
    },
    {
      id: '2',
      image: Images.flower_img2,
      heading: "lorem Ipsum is simply dummy2"
    },
    {
      id: '3',
      image: Images.flower_img3,
      heading: "lorem Ipsum is simply dummy3"
    },
    {
      id: '4',
      image: Images.flower_img1,
      heading: "lorem Ipsum is simply dummy4"
    },
    {
      id: '5',
      image: Images.img1,
      heading: "lorem Ipsum is simply dummy5"
    },
    {
      id: '6',
      image: Images.flower_img1,
      heading: "lorem Ipsum is simply dummy6"
    },
    {
      id: '7',
      image: Images.flower_img2,
      heading: "lorem Ipsum is simply dummy7"
    },
    {
      id: '8',
      image: Images.flower_img3,
      heading: "lorem Ipsum is simply dummy8"
    },
    {
      id: '9',
      image: Images.flower_img1,
      heading: "lorem Ipsum is simply dummy9"
    },
    {
      id: '10',
      image: Images.flower_img2,
      heading: "lorem Ipsum is simply dummy10"
    },
  ];

  return (
    <HomeScreen
      newsArr={newsArr}
    />
  )
}
export default Home