import { colors } from "../../../assets/styles/Colors";
import { dpFont, dpHeight, dpImageHeight, dpSpacing, dpWidth, dpImageWidth } from "../../../assets/styles/Sizes";
const { StyleSheet } = require("react-native");

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: colors.white
    },
})
export default styles;