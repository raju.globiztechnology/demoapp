import React from 'react';
import { View } from 'react-native';
import Card from '../../components/card/Card';
// import FlipPage, { FlipPagePage } from 'react-native-flip-page';
import FlipPage, { FlipPagePage } from '../../components/flip';
import styles from './styles';


const HomeScreen = (props) => {
    const { newsArr } = props;
    return (
        <View style={styles.main}>
            <FlipPage>
                {newsArr && newsArr.length > 0 && newsArr.map((item, index) => (
                    <FlipPagePage key={index}>
                        <Card
                            image={item.image}
                            heading={item.heading}
                        />
                    </FlipPagePage>
                ))}
            </FlipPage>
        </View>
    );
};

export default HomeScreen;
