import { colors } from "../../../../assets/styles/Colors";
import { dpBorderWidth, dpFont, dpHeight, dpSpacing } from "../../../../assets/styles/Sizes";
const { StyleSheet } = require("react-native");


const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: colors.white,
    },
    scroll: {
        flex: 1,
    },
    title_area: {
        flexDirection: 'row',
        paddingVertical: dpHeight(2),
        paddingHorizontal: dpHeight(1)
    },
    left_arrow: {
        flex: 0.10,
    },
    right: {
        flex: 0.90,
    },
    title: {
        fontSize: dpFont(26),
        fontWeight: "bold",
        color: colors.black,
        textAlign: "center",
    },
    bottom_area: {
        marginVertical: dpHeight(6)
    },
    flatListStyle: {
        paddingHorizontal: dpSpacing(5),
        marginRight: dpSpacing(-5)
    },
    state_view: {
        borderRadius: dpBorderWidth(10),
        backgroundColor: colors.lightGrey2,
        height: dpHeight(27),
        flex: 0.5,
        justifyContent: "center",
        alignItems: "center",
        marginRight: dpSpacing(5),
        marginVertical: dpSpacing(3),
        padding: dpSpacing(3)


    },
    state: {
        fontSize: dpFont(20),
        color: colors.black,
        textAlign: "center",
    }
})
export default styles;