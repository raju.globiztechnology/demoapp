import React from 'react';
import styles from './styles';
import { FlatList, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import IconsC from '../../../components/icons/IconsC';
import { IconsName, IconsType } from '../../../../assets/styles/Icon';
import { dpFont } from '../../../../assets/styles/Sizes';
import { colors } from '../../../../assets/styles/Colors';

const State_Screen = (props) => {
    const {
        state_arr,
        selectfun,
        selState,
        back
    } = props;
    return (
        <View style={styles.main}>
            <ScrollView style={styles.scroll} showsVerticalScrollIndicator={false}>
                <View style={styles.title_area}>
                    <TouchableOpacity
                        style={styles.left_arrow}
                        onPress={() => back()}
                    >
                        <IconsC
                            name={IconsName.arrowLeft}
                            type={IconsType.feather}
                            size={dpFont(30)}
                            color={colors.black}
                        />
                    </TouchableOpacity>
                    <View style={styles.right}>
                        <Text style={styles.title}>Select State</Text>
                    </View>
                </View>
                <View style={styles.bottom_area}>
                    <FlatList
                        data={state_arr}
                        style={styles.flatListStyle}
                        containerStyle={styles.flatContainerStyle}
                        renderItem={({ item }) => <TouchableOpacity
                            style={[selState === item.name ?
                                [styles.state_view, { backgroundColor: colors.orange }] :
                                styles.state_view]}
                            onPress={() => selectfun(item.name)}>
                            <Text style={styles.state}>{item.name}</Text>
                        </TouchableOpacity>}
                        numColumns={2}
                        keyExtractor={item => item.id}
                    />
                </View>
            </ScrollView>
        </View>
    )
}

export default State_Screen;
