import React, { useState } from 'react';
import State_Screen from './select_state';
import { useNavigation } from '@react-navigation/native';


const Select_State = (props) => {
    const state_arr = [
        {
            id: 1,
            name: 'Delhi'
        },
        {
            id: 2,
            name: 'haryana'
        },
        {
            id: 3,
            name: 'chisgarh'
        },
        {
            id: 4,
            name: 'Mumbai'
        },
        {
            id: 5,
            name: 'Himacha Pardesh'
        },
        {
            id: 6,
            name: 'Punjab'
        },
        {
            id: 7,
            name: 'Mumbai'
        },
        {
            id: 8,
            name: 'Himacha Pardesh'
        },

    ];

    const navigation = useNavigation();
    const [selState, setSelState] = useState('');

    const selectfun = (val) => {
        let stateName = val;
        setSelState(stateName);
        navigation.push('city_screen');
    };

    return (
        <State_Screen
            state_arr={state_arr}
            selectfun={selectfun}
            selState={selState}
            back={props.navigation.goBack}
        />
    )
}

export default Select_State;