import React, { useState } from 'react';
import Language_Screen from './language_screen';
import { useNavigation } from '@react-navigation/native';

const Language = () => {
    const Lang_arr = ['English', 'Hindi', 'Punjabi',
        'Telgu', 'Gujarati', 'Nepali',
        'Sindhi', 'Oriya', 'Tamil',
        'Malayalam', 'Maithili', 'Assamese'
    ];
    const navigation = useNavigation();
    const [selLang, setSelLang] = useState('');

    const selectfun = (val) => {
        let language = val;
        setSelLang(language);
        navigation.push('state_screen');
    };


    return (
        <Language_Screen
            Lang_arr={Lang_arr}
            selLang={selLang}
            selectfun={selectfun}
        />
    )
}

export default Language;
