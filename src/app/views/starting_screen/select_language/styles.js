import { colors } from "../../../../assets/styles/Colors";
import { dpBorderWidth, dpFont, dpHeight, dpSpacing } from "../../../../assets/styles/Sizes";
const { StyleSheet } = require("react-native");


const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: colors.white,
    },
    scroll: {
        flex: 1,
    },
    title_area: {
        paddingVertical: dpHeight(12),
    },
    title: {
        fontSize: dpFont(26),
        fontWeight: "bold",
        color: colors.black,
        textAlign: "center"
    },
    bottom_area: {
        backgroundColor: colors.light_purple,
        paddingVertical: dpHeight(3)
    },
    flatListStyle: {
        paddingHorizontal: dpSpacing(5),
    },
    lang_view: {
        marginBottom: dpSpacing(5),
        borderWidth: dpBorderWidth(2),
        paddingVertical: dpHeight(3),
        borderColor: colors.white,
        borderRadius: dpBorderWidth(10)
    },
    lang: {
        fontSize: dpFont(20),
        color: colors.black,
        textAlign: "center",
    }
})
export default styles;