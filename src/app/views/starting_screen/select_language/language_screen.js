import React from 'react';
import styles from './styles';
import { FlatList, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { colors } from '../../../../assets/styles/Colors';

const Language_Screen = (props) => {
    const {
        Lang_arr,
        selLang,
        selectfun
    } = props;
    return (
        <View style={styles.main}>
            <ScrollView style={styles.scroll} showsVerticalScrollIndicator={false}>
                <View style={styles.title_area}>
                    <Text style={styles.title}>Select Content Language</Text>
                </View>
                <View style={styles.bottom_area}>
                    <FlatList
                        data={Lang_arr}
                        style={styles.flatListStyle}
                        containerStyle={styles.flatContainerStyle}
                        renderItem={({ item }) => <TouchableOpacity
                            style={[selLang === item ?
                                [styles.lang_view, { backgroundColor: colors.orange }] :
                                styles.lang_view]}
                            onPress={() => selectfun(item)}>
                            <Text style={styles.lang}>{item}</Text>
                        </TouchableOpacity>}
                        keyExtractor={(item, index) => index}
                    />
                </View>
            </ScrollView>
        </View>
    )
}

export default Language_Screen;
