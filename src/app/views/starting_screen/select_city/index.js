import React, { useState } from 'react';
import City_Screen from './select_city';
import { useNavigation } from '@react-navigation/native';


const Select_City = (props) => {
    const city_arr = [
        {
            id: 1,
            name: 'Delhi'
        },
        {
            id: 2,
            name: 'Haryana'
        },
        {
            id: 3,
            name: 'Chisgarh'
        },
        {
            id: 4,
            name: 'Mumbai'
        },
        {
            id: 5,
            name: 'Himacha Pardesh'
        },
        {
            id: 6,
            name: 'Punjab'
        },
        {
            id: 7,
            name: 'Mumbai'
        },
        {
            id: 8,
            name: 'Himacha Pardesh'
        },

    ];

    const navigation = useNavigation();
    const [selCity, setSelCity] = useState('');

    const selectfun = (val) => {
        let cityName = val;
        setSelCity(cityName);
        navigation.push('home_screen');
    }

    return (
        <City_Screen
            city_arr={city_arr}
            selCity={selCity}
            selectfun={selectfun}
            back={props.navigation.goBack}
        />
    )
}
export default Select_City;