import React from 'react';
import styles from './styles';
import { FlatList, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import IconsC from '../../../components/icons/IconsC';
import { IconsName, IconsType } from '../../../../assets/styles/Icon';
import { dpFont } from '../../../../assets/styles/Sizes';
import { colors } from '../../../../assets/styles/Colors';

const City_Screen = (props) => {
    const {
        city_arr,
        selCity,
        selectfun,
        back
    } = props;
    return (
        <View style={styles.main}>
            <ScrollView style={styles.scroll} showsVerticalScrollIndicator={false}>
                <View style={styles.title_area}>
                    <TouchableOpacity
                        style={styles.left_arrow}
                        onPress={() => back ? back() : null}
                    >
                        <IconsC
                            name={IconsName.arrowLeft}
                            type={IconsType.feather}
                            size={dpFont(30)}
                            color={colors.black}
                        />
                    </TouchableOpacity>
                    <View style={styles.right}>
                        <Text style={styles.title}>Select City</Text>
                    </View>
                </View>
                <View style={styles.bottom_area}>
                    <FlatList
                        data={city_arr}
                        style={styles.flatListStyle}
                        containerStyle={styles.flatContainerStyle}
                        renderItem={({ item }) => <TouchableOpacity
                            style={[selCity === item.name ?
                                [styles.city_view, { backgroundColor: colors.orange }] :
                                styles.city_view]}
                            onPress={() => selectfun(item.name)}
                        >
                            <Text style={styles.city}>{item.name}</Text>
                        </TouchableOpacity>}
                        keyExtractor={item => item.id}
                    />
                </View>
            </ScrollView>
        </View>
    )
}
export default City_Screen;
