import { colors } from "../../../../assets/styles/Colors";
import { dpBorderWidth, dpFont, dpHeight, dpImageHeight, dpImageWidth } from "../../../../assets/styles/Sizes";
const { StyleSheet } = require("react-native");

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: colors.white,
        justifyContent: "center",
        alignItems: "center"
    },
    imgContainer: {
        height: dpImageHeight(120),
        width: dpImageWidth(120),
        borderRadius: dpBorderWidth(7),
        overflow: "hidden"
    },
    imgStyle: {
        height: '100%',
        width: '100%'
    },
    title: {
        fontSize: dpFont(30),
        fontWeight: "bold",
        color: colors.orange,
        textTransform: "uppercase"
    },
    desc: {
        fontSize: dpFont(24),
        fontWeight: "bold",
        color: colors.black,
        marginTop: dpHeight(13)
    }
})
export default styles;