import React from 'react';
import styles from './styles';
import { Image, Text, View } from 'react-native';
import Images from '../../../../assets/styles/Images';

const SplashScreen = () => {
    return (
        <View style={styles.main}>
            <View style={styles.imgContainer}>
                <Image
                    source={Images.logo}
                    style={styles.imgStyle}
                />
            </View>
            <Text style={styles.title}>AnyNews</Text>
            <Text style={styles.desc}>The Best Short News App</Text>
        </View>
    )
}

export default SplashScreen;
