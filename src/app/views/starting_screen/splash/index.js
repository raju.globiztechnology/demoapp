import React, { useEffect } from 'react';
import SplashScreen from './splash';
import { useNavigation } from '@react-navigation/native';

const Splash = () => {
  const navigation = useNavigation();

  useEffect(() => {
    const timer = setTimeout(() => {
      navigation.reset({
        index: 0,
        routes: [
          { name: 'language_screen' },
        ],
      })
    }, 3000)
    return () => {
      clearTimeout(timer);
    };
  });

  return (
    <SplashScreen />
  )
}

export default Splash;
