import React from 'react'
import IconsC from '../icons/IconsC'
import { IconsName, IconsType } from '../../../assets/styles/Icon'
import { colors } from '../../../assets/styles/Colors'
import { dpFont } from '../../../assets/styles/Sizes'
import Images from '../../../assets/styles/Images'
import { View, Text, Image, TouchableOpacity } from 'react-native'

import styles from './styles'

const Card = (props) => {
    const {
        image,
        heading
    } = props
    return (
        <View style={styles.main}>
            <View style={styles.topView}>
                <View style={styles.main_img}>
                    <Image
                        style={styles.img}
                        source={image ? image : Images.img1}
                    />
                </View>
                <View style={styles.socials_icon}>
                    <TouchableOpacity style={styles.likeIcon} onPress={() => console.log('like click')}>
                        <IconsC
                            name={IconsName.like}
                            type={IconsType.evilIcons}
                            size={dpFont(32)}
                            color={colors.grey}
                        />
                    </TouchableOpacity >
                    <TouchableOpacity style={styles.likeIcon} onPress={() => console.log('message click')}>
                        <IconsC
                            name={IconsName.messageCircle}
                            type={IconsType.feather}
                            size={dpFont(23)}
                            color={colors.grey}
                        />
                    </TouchableOpacity >
                    <TouchableOpacity style={styles.likeIcon} onPress={() => console.log('share click')}>
                        <IconsC
                            name={IconsName.share}
                            type={IconsType.ionIcon}
                            size={dpFont(23)}
                            color={colors.grey}
                        />
                    </TouchableOpacity >
                    <TouchableOpacity style={styles.likeIcon} onPress={() => console.log('exclamation click')}>
                        <IconsC
                            name={IconsName.exclamation}
                            type={IconsType.fontAwesome6}
                            size={dpFont(23)}
                            color={colors.grey}
                        />
                    </TouchableOpacity >
                </View>
            </View>
            <View style={styles.bottomView}>
                <Text style={styles.heading}>
                {heading ? heading : null}
                </Text>
                <Text style={styles.desc}>
                    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'   It is a long established fact that established fact.
                </Text>

                <View style={styles.info}>
                    <View style={styles.left}>
                        <View style={styles.icon}>
                            <IconsC
                                name={IconsName.user}
                                type={IconsType.fontAwesome}
                                color={colors.white}
                                size={dpFont(22)}
                            />
                        </View>
                        <View style={styles.infoInner}>
                            <Text style={styles.name}>Shaiker Nazeer</Text>
                            <View style={styles.detail}>
                                <Text style={styles.pos}>Repoter</Text>
                                <Text style={styles.time}>11 min ago</Text>
                            </View>
                        </View>
                        <Text style={styles.follow}>Follow</Text>
                    </View>
                    <View style={styles.right}>
                        <View style={styles.loc}>
                            <IconsC
                                name={IconsName.locationDot}
                                type={IconsType.fontAwesome6}
                                size={dpFont(12)}
                                color={colors.orange}
                            />
                            <Text style={styles.city}>New Delhi</Text>
                        </View>
                    </View>
                </View>
            </View>

        </View>
    )
}

export default Card
