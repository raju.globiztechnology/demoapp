
// import { colors } from "../../../assets/styles/Colors";
import { colors } from "../../../assets/styles/Colors";
import { dpBorderWidth, dpFont, dpHeight, dpImageHeight, dpSpacing, dpWidth, dpImageWidth } from "../../../assets/styles/Sizes";
const { StyleSheet } = require("react-native");

const styles = StyleSheet.create({
    main: {
        // height: '100%',
        flex: 1,
        backgroundColor: colors.white
    },
    topView: {
        position: "relative"
    },
    main_img: {
        height: dpImageHeight(250),
        width: '100%',
    },
    img: {
        height: '100%',
        width: '100%'
    },
    bottomView: {
        paddingHorizontal: dpSpacing(3)
    },
    heading: {
        color: colors.black,
        fontWeight: '700',
        fontSize: dpFont(20),
        paddingTop: dpHeight(3.3)
    },
    desc: {
        color: colors.black,
        fontWeight: '400',
        fontSize: dpFont(17),
        paddingTop: dpHeight(1.6),
        lineHeight: 22
    },
    info: {
        paddingTop: dpSpacing(4),
        flexDirection: "row"
    },
    left: {
        flexDirection: "row",
        flex: 0.68,
    },
    icon: {
        height: dpHeight(5),
        width: dpWidth(9),
        borderRadius: dpHeight(5) / 2,
        backgroundColor: colors.blue,
        justifyContent: "center",
        alignItems: "center"
    },
    infoInner: {
        paddingLeft: dpSpacing(2)
    },
    name: {
        color: colors.black,
        fontSize: dpFont(11),
        fontWeight: 'bold',
    },
    detail: {
        flexDirection: "row",
        alignItems: "center",
        paddingTop: dpHeight(1.5),
    },
    pos: {
        color: colors.black,
        fontSize: dpFont(8),
        fontWeight: 'bold',
    },
    time: {
        color: colors.black,
        fontSize: dpFont(8),
        paddingLeft: dpSpacing(3),
        fontWeight: 'bold',
    },
    follow: {
        color: colors.orange,
        fontWeight: 'bold',
        fontSize: dpFont(10),
        paddingLeft: dpSpacing(5),
        paddingTop: dpSpacing(2.5)
    },
    right: {
        flex: 0.32,
    },
    loc: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        borderWidth: dpBorderWidth(1),
        borderColor: colors.grey,
        borderRadius: dpFont(40),
        paddingHorizontal: dpSpacing(2),
        paddingVertical: dpSpacing(4)
    },
    city: {
        fontSize: dpFont(16),
        color: colors.black,
        fontWeight: "bold",
        paddingLeft: dpSpacing(3)
    },
    socials_icon: {
        flexDirection: "row",
        justifyContent: "flex-end",
        position: "absolute",
        bottom: 0,
        marginBottom: dpHeight(-3),
        right: 0
    },
    likeIcon: {
        backgroundColor: colors.lightGrey2,
        height: dpHeight(6),
        width: dpWidth(11),
        borderRadius: dpHeight(6) / 2,
        justifyContent: "center",
        alignItems: "center",
        marginRight: dpSpacing(3),
        zIndex: 99
    }
})
export default styles;