import React from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import FontAwesome6 from 'react-native-vector-icons/FontAwesome6'
import Fontisto from 'react-native-vector-icons/Fontisto'
import Foundation from 'react-native-vector-icons/Foundation'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Octicons from 'react-native-vector-icons/Octicons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import Zocial from 'react-native-vector-icons/Zocial'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

const IconsC = (props) => {
  const {
    name,
    type,
    size,
    color 
  } = props

  return (
    <>
      {type === "antDesign" && <AntDesign name={name} color={color} size={size} />}
      {type === "entypo" && <Entypo name={name} color={color} size={size} />}
      {type === "evilIcons" && <EvilIcons name={name} color={color} size={size} />}
      {type === "feather" && <Feather name={name} color={color} size={size} />}
      {type === "FontAwesome" && <FontAwesome name={name} color={color} size={size} />}
      {type === "fontAwesome5" && <FontAwesome5 name={name} color={color} size={size} />}
      {type === "FontAwesome6" && <FontAwesome6 name={name} color={color} size={size} />}
      {type === "fontisto" && <Fontisto name={name} color={color} size={size} />}
      {type === "foundation" && <Foundation name={name} color={color} size={size} />}
      {type === "ionIcons" && <Ionicons name={name} color={color} size={size} />}
      {type === "materialCommunityIcons" && <MaterialCommunityIcons name={name} color={color} size={size} />}
      {type === "octicons" && <Octicons name={name} color={color} size={size} />}
      {type === "simpleLineIcons" && <SimpleLineIcons name={name} color={color} size={size} />}
      {type === "zocial" && <Zocial name={name} color={color} size={size} />}
      {type === "materialIcons" && <MaterialIcons name={name} color={color} size={size} />}
    </>
  )
}
export default IconsC
