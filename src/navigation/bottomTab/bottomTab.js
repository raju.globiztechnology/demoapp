import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import IconsC from '../../app/components/icons/IconsC';
import { View, Image } from 'react-native';
import styles from './styles';
import Images from '../../assets/styles/Images';
import Home from '../../app/views/home';
import { IconsName, IconsType } from '../../assets/styles/Icon';
import { colors } from '../../assets/styles/Colors';
import { dpFont, dpSpacing } from '../../assets/styles/Sizes';
const Tab = createBottomTabNavigator();


const BottomTab = () => {
    return (
        <Tab.Navigator
            initialRouteName={'home'}
            // headerShown={false}
            screenOptions={{
                headerShown: false,
                tabBarShowLabel: true,
                tabBarActiveTintColor: colors.orange,
                tabBarLabelStyle: {
                    fontSize: dpFont(14),
                    marginTop: dpSpacing(-2),
                    marginBottom: dpSpacing(2),
                    padding: 0,
                },
                tabBarStyle: {
                    backgroundColor: colors.lightGrey,
                    height: 55,
                    borderTopWidth: 0,
                    elevation: 10
                }
            }}>
            <Tab.Screen
                name="home"
                component={Home}
                options={{
                    tabBarLabel: 'Reload',
                    tabBarIcon: ({ focused }) => (
                        <IconsC
                            name={IconsName.reload}
                            type={IconsType.simpleLineIcons}
                            size={24}
                            color={focused ? colors.orange : colors.grey}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="search"
                component={Home}
                options={{
                    tabBarLabel: 'Organize',
                    tabBarIcon: ({ focused }) => (
                        <IconsC
                            name={IconsName.like}
                            type={IconsType.evilIcons}
                            size={34}
                            color={focused ? colors.orange : colors.grey}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="whatsap"
                component={Home}
                options={{
                    tabBarLabel: '',
                    tabBarShowLabel: false,
                    tabBarIcon: () => (
                        <View style={styles.whatsap_logo_main}>
                            <Image source={Images.whatsapp} style={styles.img} />
                        </View>
                    ),
                }}
            />
            <Tab.Screen
                name="categories"
                component={Home}
                options={{
                    tabBarLabel: 'Category',
                    tabBarIcon: ({ focused }) => (
                        <View style={styles.cateImg_main}>
                            <Image source={focused ? Images.categories2 : Images.categories}
                                style={styles.img}
                            />
                        </View>
                    ),
                }}
            />
            <Tab.Screen
                name="profile"
                component={Home}
                options={{
                    tabBarLabel: 'Profile',
                    tabBarIcon: ({ focused }) => (
                        <IconsC
                            name={IconsName.user}
                            type={IconsType.fontAwesome}
                            size={dpFont(30)}
                            color={focused ? colors.orange : colors.grey}
                        />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}

export default BottomTab;