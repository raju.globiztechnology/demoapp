import { dpImageHeight, dpImageWidth, dpSpacing } from "../../assets/styles/Sizes";
const { StyleSheet } = require("react-native");


const styles = StyleSheet.create({
    whatsap_logo_main: {
        height: dpImageHeight(45),
        width: dpImageWidth(45),
        marginTop: dpSpacing(4)
    },
    img: {
        height: '100%',
        width: '100%'
    },
    cateImg_main: {
        height: dpImageHeight(23),
        width: dpImageWidth(23),
    },
})
export default styles;












