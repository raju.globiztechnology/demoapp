import React from 'react'
const Stack = createStackNavigator();
import Language from '../../app/views/starting_screen/select_language';
import Select_State from '../../app/views/starting_screen/select_state';
import Select_City from '../../app/views/starting_screen/select_city';
import BottomTab from '../bottomTab/bottomTab';
import { createStackNavigator } from '@react-navigation/stack';
import Splash from '../../app/views/starting_screen/splash';


const StackNavigation = () => {
  return (
    <>
      <Stack.Navigator
        initialRouteName='Splash'
        screenOptions={{
          // headerShown: false
        }}
      >
        <Stack.Screen
          name="splash"
          component={Splash}
          options={{
            headerShown: false
            // header: ({ navigation, route, options }) => (
            //   <Header

            //   />
            // )
          }
          }
        />
        <Stack.Screen
          name="language_screen"
          component={Language}
          options={{
            headerShown: false
            // header: ({ navigation, route, options }) => (
            //   <Header

            //   />
            // )
          }
          }
        />
        <Stack.Screen
          name="state_screen"
          component={Select_State}
          options={{
            headerShown: false
            // header: ({ navigation, route, options }) => (
            //   <Header

            //   />
            // )
          }
          }
        />
        <Stack.Screen
          name="city_screen"
          component={Select_City}
          options={{
            headerShown: false
            // header: ({ navigation, route, options }) => (
            //   <Header

            //   />
            // )
          }
          }
        />
        <Stack.Screen
          name="home_screen"
          component={BottomTab}
          options={{
            headerShown: false
            // header: ({ navigation, route, options }) => (
            //   <Header

            //   />
            // )
          }
          }
        />



      </Stack.Navigator>
    </>
  )
}

export default StackNavigation
