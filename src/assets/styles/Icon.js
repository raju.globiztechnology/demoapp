// Names of Icons Used in Application


export const IconsName = {
  //-------------------------->antDesign

  //-------------------------->FontAwesome
  user: 'user',

  //-------------------------->EvilIcons
  like: "like",

  //-------------------------->SimpleLineIcons
  reload: "reload",
  locationPin: "location-pin",

  //-------------------------->Feather
  userOutLine: "user",
  messageCircle: "message-circle",
  arrowLeft: 'arrow-left',

  //-------------------------->Ionicons
  share: "share-social-outline",

  //-------------------------->FontAwesome6
  exclamation: "exclamation",
  locationDot: "location-dot",

  //-------------------------->MaterialCommunityIcons
  exclamation: "exclamation",

};



// Types of Icons
export const IconsType = {
  fontAwesome: 'FontAwesome',
  fontAwesome5: 'fontAwesome5',
  fontAwesome6: 'FontAwesome6',
  antDesign: 'antdesign',
  entypo: 'entypo',
  evilIcons: 'evilIcons',
  feather: 'feather',
  fontisto: 'fontisto',
  foundation: 'foundation',
  ionIcon: 'ionIcons',
  materialIcons: 'materialIcons',
  materialCommunityIcons: 'materialCommunityIcons',
  octicons: 'octicons',
  simpleLineIcons: 'simpleLineIcons',
  zocial: 'zocial',
};
