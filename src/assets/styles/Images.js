const Images = {
    img1: require('../images/blog_detail.png'),
    img2: require('../images/blog_9.png'),
    whatsapp: require('../images/whatsapp.png'),
    categories: require('../images/categories.png'),
    categories2: require('../images/categories2.png'),
    flower_img1: require('../images/img1.jpg'),
    flower_img2: require('../images/img2.jpg'),
    flower_img3: require('../images/img3.jpg'),
    logo: require('../images/logo.png'),
};

export default Images;