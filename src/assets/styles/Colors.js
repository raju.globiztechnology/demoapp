export const colors = {
    black: 'black',
    white: 'white',
    grey: "#A3A3A4",
    darkGrey: "#282828",
    blue: "#009FE3",
    lightGrey: "#F8F8F8",
    lightGrey2: "#F5F7FB",
    orange: "#EB552A",
    light_purple: "#F5F7FB",

};